# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

echo "Sourcing .bashrc"

# You'd like to be able to see the datetime that a command was executed, such as if you try to `ssh`
# into an unresponsive machine several times and are curious when the last time you tried and failed
# was (or the first time you tried and failed was). Hence, you're turning `ignoredups` off.
#
# You don't know what the value is in getting rid of commands starting with a space is, so you're
# also turning `ignorespace` off.
#
# See bash(1) for details
HISTCONTROL=

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=10000
HISTFILESIZE=20000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
# https://www.linuxjournal.com/content/globstar-new-bash-globbing-option
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# Moved this to /usr/bin/sc so that you can run it with Alt+F2 in Ubuntu (not leave a terminal lying
# around, see https://askubuntu.com/a/740174/612216).
# alias sc='scrcpy -m 1024'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Since you regularly use BTRFS filesystems; see also "Default to CoW" in your notes or:
# - https://unix.stackexchange.com/questions/80351
alias cp='cp --reflink=auto --sparse=always'

# See the "Reboot vdd" reminder.
alias rs='watch -n0 resize -s 194 720'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# Set environment variables it is OK to set multiple times and that are
# convenient to have in a docker container. For more details, see comments
# in .profile.

# There are two ways to get colored man pages; start with `most` and if this
# doesn't work out try the other approach:
# - https://www.howtogeek.com/683134/how-to-display-man-pages-in-color-on-linux/
# This doesn't work with `git diff` nicely, which has its own colors (looks
# terrible with these colors):
# export PAGER='most'

# This question specifically mentions putting EDITOR into your .bashrc file:
# - https://superuser.com/a/789465/293032
export EDITOR='vim'

# See the suggested NINJA_STATUS here:
# https://ninja-build.org/manual.html#_environment_variables
export NINJA_STATUS="[%u/%r/%f] "

# You were having issues with UTF-8 characters showing up as junk in files you
# were editing in docker images with vim. The vim documentation suggests
# getting your system locale right, which it will then use. You would otherwise
# have to set many VIM variables:
# - https://stackoverflow.com/questions/5166652/
# - http://vimdoc.sourceforge.net/htmldoc/mbyte.html
# - https://stackoverflow.com/a/1459612/622049
# - https://stackoverflow.com/questions/126853/
#
# It seems most docker images use the POSIX locale (equivalent to the C locale)
# which is apparently not UTF-8. For more on the C locale see:
# - https://www.man7.org/linux/man-pages/man3/setlocale.3.html
# - https://unix.stackexchange.com/questions/87745/what-does-lc-all-c-do
#
# Use `locale -a` to show all available locals a system. In the docker image
# you were looking at (which was pretty minimal) the C.UTF-8 locale was also
# available. You're assuming it is otherwise mostly available.
export LANG=C.UTF-8

# See:
# - https://developer.okta.com/blog/2021/07/07/developers-guide-to-gpg#enable-your-gpg-key-for-ssh
#
# Simply adding this line to your .bashrc assigns the variable when you're connected over ssh,
# however, which is undesirable unless you also forward the `S.gpg-agent.ssh` socket (undesirable
# for now). This custom solution checks if we're on an ssh connection before updating the variable.
if [ -z "${SSH_CONNECTION+x}" ] ; then
    export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
fi

# To compare this file to the original:
# vimdiff /etc/skel/.bashrc .bashrc
