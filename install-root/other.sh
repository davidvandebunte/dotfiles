#!/usr/bin/env bash
set -e

apt-get install --yes --no-install-recommends build-essential

apt-get install --yes --no-install-recommends \
    ccache \
    clang-6.0 \
    clang-format-6.0 \
    clang-tidy-6.0

# Primarily for clang-tidy
apt-get install --yes --no-install-recommends \
    clang-tools-6.0 \
    curl \

# OpenMP is required for many C++ programs
# https://askubuntu.com/a/903982/612216
apt-get install --yes --no-install-recommends \
    libomp-dev

# SSL is required to compile many C++ programs
apt-get install --yes --no-install-recommends \
    libssl-dev \
    openjdk-8-jre \
    python3 python3-pip python3-setuptools

apt-get install --yes --no-install-recommends \
    tree \
    unzip

# Install -gtk3 version to get +clipboard
# https://stackoverflow.com/a/11489440/622049
apt-get install --yes --no-install-recommends \
    vim-gtk3 \
    wget

# The package clang-tools-6.0 installs an executable named clang-tidy-6.0 and puts
# it on the PATH. ALE looks for an executable named "clang-tidy" (without the
# version on the end). Add this mapping so ALE finds clang-tidy; similarly for
# clang-check.
update-alternatives --install /usr/bin/clang-check clang-check /usr/bin/clang-check-6.0 100
update-alternatives --install /usr/bin/clang-tidy  clang-tidy  /usr/bin/clang-tidy-6.0  100
