#!/usr/bin/env bash
set -e

# Install -gtk3 version to get +clipboard
# https://stackoverflow.com/a/11489440/622049
apt-get install --yes --no-install-recommends \
    curl \
    vim-gtk3 \
    wget

# https://github.com/amperser/proselint
apt-get install --yes --no-install-recommends \
    python3-proselint

wget https://github.com/redpen-cc/redpen/releases/download/redpen-1.10.1/redpen-1.10.1.tar.gz \
 && tar xvf redpen-1.10.1.tar.gz

# For the list of available ale linters, see :help ale-support.
# https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-18-04#installing-using-a-ppa
# https://github.com/w0rp/ale/blob/master/ale_linters/markdown/markdownlint.vim
# https://github.com/DavidAnson/markdownlint
# https://github.com/igorshubovych/markdownlint-cli
curl -sL https://deb.nodesource.com/setup_10.x -o nodesource_setup.sh \
 && bash nodesource_setup.sh \
 && apt-get install --yes --no-install-recommends nodejs \
 && npm install markdownlint --save-dev \
 && npm install -g markdownlint-cli
