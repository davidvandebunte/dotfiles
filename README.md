# Background


These instructions are based on the simple approach advocated by the article [How to store dotfiles | Atlassian Git Tutorial][htsd]. Some advantages of publicly shared dotfiles:
- Anonymously clone as long as you use https
- Unlimited CI/CD minutes in GitLab

[htsd]: https://developer.atlassian.com/blog/2016/02/best-way-to-store-dotfiles-git-bare-repo/


They're rather tied to Ubuntu although it'd be nice to support to Red Hat, Debian, and other Linux Desktops.


## Detailed instructions


- [Set up Ubuntu Server](./bin/set-up-server.md)
- [Set up Ubuntu Desktop](./bin/set-up-desktop.md)
