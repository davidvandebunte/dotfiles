#!/usr/bin/env bash
set -Eeux

# We must install the dotfiles to a ".cfg" directory because ".cfg"
# is hard-coded into the .bash_aliases file in the dotfiles we check
# out.
git clone --bare "$HOME/dotfiles" "$HOME/.cfg"
dotf() { /usr/bin/git --git-dir="$HOME/.cfg/" --work-tree="$HOME" $@ ; }

mkdir -p ~/.gnupg
chmod 700 ~/.gnupg
# https://stackoverflow.com/a/8296746/622049
dotf checkout 2>&1 | egrep '^\s' | awk {'print $1'} | xargs -r rm
dotf checkout
dotf config --local status.showUntrackedFiles no
dotf config --local user.email davidvandebunte@gmail.com
dotf config --local user.name "David VandeBunte"

# TODO: Clone git repositories, not submodules. Clone these repositories
# only as needed.
#
# For example, the git submodules you need here for vim should be cloned
# when you setup vim.
#
# Disabling to avoid issues with lack of ssh support in CI/CD.
# dotf submodule update --init --recursive

# https://stackoverflow.com/a/36410649/622049
dotf config remote.origin.fetch '+refs/heads/*:refs/remotes/origin/*'
dotf fetch
dotf remote rename origin local
dotf co -b local
mkdir ~/mount

if type xrdb > /dev/null 2>&1; then
    if [ -n "$DISPLAY" ]; then
        xrdb -merge ~/.Xresources
    fi
fi
