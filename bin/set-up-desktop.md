---
jupytext:
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.6
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Set up [Ubuntu Desktop](https://ubuntu.com/download/desktop)

+++

## Use Cases

+++

Why switch to a new workstation (with new monitors, etc.)? Perhaps to:
- Migrate to a new operating system (e.g. `focal` to `jammy`). While `do-release-upgrade` is often the easiest option, a new system allows changing filesystems and saving disk space.
- Verify your system is backed up and reproducible, to avoid data loss. The more often you move, the more backups you have.
- Move to a new machine that can support a bigger or more monitors.
- Migrate to a newly-obtained personal laptop or desktop.
- Change companies (perhaps both a laptop and desktop).
- Reinstall your operating system because software gets permanently broken (usually accidentally):
  - Jan 2024: Attempt to update to a new Ubuntu version fails on your laptop.
  - April 2024: An Ubuntu 20.04 update randomly breaks your main work system.
- Update to a new Ubuntu version in WSL.
- Set up a new machine after a hardware failure.

+++

The value in switching back and forth between two local machines (e.g. `david-desktop-green` and `david-desktop-blue`) more frequently is in one or multiple of the items above.

+++

## Time estimate

+++

One day to several, depending on how recently you've followed this process.

+++

## Double checks

+++

### Unsaved files

+++

Consider running `dotf config --local status.showUntrackedFiles normal` to see untracked files (regularly set to `no`, see [status.showUntrackedFiles](https://git-scm.com/docs/git-config#Documentation/git-config.txt-statusshowUntrackedFiles)). You can add files you know you don't want to move to `.gitignore` to make your checks easier next time.

Your offical policy is that `Documents` should stay empty; regularly move files you know you can easily recover that were placed in that directory to `Downloads`. All files in `Downloads` should be removable at any point (like `/tmp`) but for convenience are persisted through restarts.

+++

### Look for forgotten packages

+++

See [apt - How to list manually installed packages?](https://askubuntu.com/questions/2389/how-to-list-manually-installed-packages). Use this list to temporarily compare the packages from an old machine to a new one (if you want to be sure you aren't missing anything):

+++

```bash
comm -23 <(apt-mark showmanual | sort -u) <(gzip -dc /var/log/installer/initial-status.gz | sed -n 's/^Package: //p' | sort -u) > ~/vdd-packages.txt
scp /home/vandebun/vdd-packages.txt m2:
```

+++

On the new machine:

+++

```bash
comm -23 <(apt-mark showmanual | sort -u) <(gzip -dc /var/log/installer/initial-status.gz | sed -n 's/^Package: //p' | sort -u) > ~/m2-packages.txt
comm m2-packages.txt vdd-packages.txt
```

+++

Not all packages will be "properly" listed as you're changing operating systems. For example, you had to install `libreoffice-writer` manually in 20.04 but it was system-installed in 22.04. Marking it as manually installed doesn't work because of the check of the `initial-status.gz` file above.

+++

If you're forgotten a package then you apparently don't have a use for it at the moment, though. Until you find a use, it may not be worth installing. Get on the new machine, and install it when you need it!

+++

### Secrets

+++

Review the secrets reported by `seahorse` (see [How do I check if my OpenPGP key is in the Ubuntu keyserver?](https://askubuntu.com/questions/29889/how-do-i-check-if-my-openpgp-key-is-in-the-ubuntu-keyserver)). Are all of them pushed? They'll need to be before you perform your final migration from one GUI machine to another.

+++

### Run `ssh` backwards

+++

Check you can `ssh` from the new machine to the old machine, if and only if you feel it's likely you'll leave something behind. Ironically, you may want to `ssh-copy-id` from your current machine into itself before the move.

+++

It's tricky to forward an SSH identity using a GPG agent. See [gnupg - How can I forward a gpg key via ssh-agent? - Super User](https://superuser.com/questions/161973/how-can-i-forward-a-gpg-key-via-ssh-agent). In your experience, and according to [this answer](https://unix.stackexchange.com/a/670247/233125), adding `-o "StreamLocalBindUnlink=yes"` as suggested in one answer doesn't work the second time you log in (without removing the advertised file). You must actually add this setting to `/etc/ssh/sshd_config` and run `service sshd restart`. From `man ssh_config`:

> Specifies whether to remove an existing Unix-domain socket file for local or remote port forwarding before creating a new one.  If the socket file already exists and StreamLocalBindUnlink is not enabled, ssh will be unable to forward the port to the Unix-domain socket file.  This option is only used for port forwarding to a Unix-domain socket file.

+++

From [AgentForwarding - GnuPG wiki](https://wiki.gnupg.org/AgentForwarding):

> If you can modify the the remote machine settings you should put into /etc/ssh/sshd_config the following:
>
> ```
> StreamLocalBindUnlink yes
> ```
>
> This enables automatic removal of stale sockets when connecting to the remote machine. Otherwise you will first have to remove the socket on the remote machine before forwarding works.

+++

Unfortunately, this setting creates a problem when you *aren't* connecting to a remote machine, but testing `ssh` by logging into the same machine. If you're on e.g. `m2` and have this configured to `yes`, then an `ssh m2` will actually kill your `gpg-agent` (leading to other problems). You can see this is happening in `/var/log/syslog` with lines like the following:

```
gpg-agent[32111]: socket file has been removed - shutting down
gpg-agent[32111]: gpg-agent (GnuPG) 2.2.27 stopped
```

+++

## Pre-move manual installs

+++

Run `idempotent-desktop-set-up` initially and then regularly as you add packages to the system, recording new packages you want on new systems in this script.

+++

### Zscaler

+++

You can't get Zscaler from the internet; see [Understanding Zscaler Client Connector App Downloads | Zscaler](https://help.zscaler.com/client-connector/understanding-zscaler-client-connector-app-downloads):

> There are no download links for Zscaler Client Connector in this article. This article applies only to admins for the Zscaler Client Connector Portal. If you are an end user, contact your organization’s IT admin, IT support team, or equivalent for information about installing Zscaler Client Connector.

So, plan to `scp` this file to any new machine. The link to get company-specific deb files is [Zscaler Client Connector Install Reference - Trust](https://here-technologies.atlassian.net/wiki/spaces/SPC/pages/669784768/Zscaler+Client+Connector+Install+Reference) at the moment. See also [Supported Versions | Zscaler](https://help.zscaler.com/eos-eol/supported-versions). Be sure to *reboot* after this installation; the software is unstable (see also [this comment](https://here-technologies.atlassian.net/wiki/spaces/SPC/pages/669784768/Zscaler+Client+Connector+Install+Reference?focusedCommentId=869645640)). Your username is `david.vandebunte@here.com` (not `vandebun`).

+++

The Zscaler client will let you view internal websites. To also allow reaching these websites from a docker build (e.g. Artifactory), see [this answer](https://stackoverflow.com/a/44184814/622049) (which points to [Configuring options for docker run](https://stackoverflow.com/questions/44184496/configuring-options-for-docker-run/44184773#44184773)). A working `/etc/docker/daemon.json`:

```
{
    "dns": ["100.64.0.2"]
}
```

+++

See also [Zscaler User Guide](https://confluence.in.here.com/display/SPC/Zscaler+User+Guide) and [Adding Custom Certificate to an Application Specific Trust Store | Zscaler](https://help.zscaler.com/zia/adding-custom-certificate-application-specific-trust-store); hopefully adding the root certificate to trust stores isn't necessary.

+++

### Copy `/etc/hosts`

+++

Copy the appropriate parts of the `/etc/hosts` file between machines by simply using the clipboard (`scp` would copy the whole file, while you only want a part). Hopefully Deco will eventually support internal DNS resolution (otherwise configure an internal DNS resolver and point Deco to it).

+++

## Physical move

+++

It's always going to be much harder to quickly rebase onto a [Workstation](https://en.wikipedia.org/wiki/Workstation) (an Ubuntu Desktop machine) because you need to have two copies of *all* hardware, or something close. If you have 4 monitors, this is impractical and you end up still having a "big move" when you go from e.g. the new/old machines having 1/4 monitors to 4/1 monitors. To make this switch regularly and more easily, keep up all 5 monitors all the time and have cords that are long enough to let you switch monitors without moving machines.

+++

It should not be hard to have two copies of keyboards/mice/YubiKeys, connected to a hub in a monitor so they can be switched together with one USB cable. Because of MST limitations, at least two DP cables will need to be switched; see [DisplayPort § Multi-Stream Transport (MST) ](https://en.wikipedia.org/wiki/DisplayPort#Multi-Stream_Transport_(MST)). This supports up to 2 of HERE's standard 27 inch 4k monitors. Unfortunately at least some of your monitors seem to have intermittent flashing problems when used an MST intermediary; by flipping which is the first and the second in a chain you've been able to fix this problem (perhaps this is a mix of software and hardware issues).

+++

In practice, Ubuntu NVIDIA drivers do not react well to monitors being switched in and out on a live machine. To rearrage monitors, you often need to reboot, so it likely makes sense to turn off both computers at once and switch the new workstation to the set of e.g. four new monitors and the old workstation to a single monitor.

+++

## Post-move manual installs

+++

### Ubuntu

+++

Run `gnome-control-center` (optionally over ssh) to set up Ctrl-Alt-T to open uxterm rather than Terminal. To get the name `gnome-control-center` you ran `xlsclients` on the client machine after opening "Settings" to determine what the name of the application was (see [Get a list of open windows in Linux](https://superuser.com/questions/176754/get-a-list-of-open-windows-in-linux/176761#176761)).

+++

Run `gnome-tweaks` (optionally over ssh) to remap caps lock to esc. Even if you were to use caps lock once a month (in practice you never use it) you'd be better off with it mapped to Esc (which you use so much more). See:
- [keyboard - Remapping Caps Lock to Escape in Ubuntu 18.04 bionic - Ask Ubuntu](https://askubuntu.com/questions/1059663/remapping-caps-lock-to-escape-in-ubuntu-18-04-bionic)
- [scroll - How to move screen without moving cursor in Vim? - Stack Overflow](https://stackoverflow.com/questions/3458689/how-to-move-screen-without-moving-cursor-in-vim#comment75179589_3458821)

+++

To keep a laptop running in 24.04 after the screen is closed, run the following and then reboot:

```bash
sudo sed -i 's|#HandleLidSwitchExternalPower=suspend|HandleLidSwitchExternalPower=ignore|g' /etc/systemd/logind.conf
```

+++

### Firefox

+++

Run the one-liner from [PassFF/passff-host](https://codeberg.org/PassFF/passff-host) to install the `passff-host`:

```bash
curl -sSL https://codeberg.org/PassFF/passff-host/releases/download/latest/install_host_app.sh | bash -s -- firefox
```

+++

Add the LastPass extension. Get the password for Firefox Sync from the vault and add it to Firefox. Or add the Firefox Sync password to `pass`?

+++

### Keyboard shortcuts

+++

Only save the keyboard shortcuts that you remember; you'd prefer to be using standard shortcuts and these can change (or you can easily discover more standards you were unaware of). For example, you once mapped Shift-Alt-W to a "work screenshot" that put a screenshot into some hard-coded directory. This turned out to be the shortcut for "unflow text" (similar to Alt-W) in Inkscape.

+++

See `/usr/bin/my-screenshot` (perhaps use Ctrl-Shift-S, and document that choice in the file), or just review all the keyboard shortcuts you've put in. These scripts should be in your `~/bin` directory and tracked with `dotf`, but it seems that keyboard configuration linking to them is hard to automate. For now, keep them in `~/bin` and copy them to `/usr/bin` as part of installing system packages (need to script this).

+++

You can paste into Jupyter quickly with [Is it possible to PASTE image into Jupyter Notebook? - Stack Overflow](https://stackoverflow.com/questions/40705032/is-it-possible-to-paste-image-into-jupyter-notebook). The issue is that this doesn't seem to work with `.md` files. What's the advantage of always using a globally unique filename in a `raster` directory? You can copy and paste text that refers to the file (in Markdown) to another Markdown document and have everything still work. If the data was "attached" to the Jupyter notebook then you would have to be sure to always copy/paste Jupyter cells instead (which prevents you from only taking part of a Markdown cell's text).

+++

### Long-term installs

+++

Once you've been working on the new machine for a while and feel that it's going to function long term:
- Set up DuckDuckGo with your personal preferences (LastPass will auto-fill this otherwise easy to remember password for you).
- Set up "Export Tabs" and other Firefox extensions, pinning them to where you'd like them on the toolbar. Use the puzzle piece (🧩) to open up the list of extensions, then the gear (⚙️) next to each extension to pin it. Once it's pinned you can right-click on the toolbar to customize the button locations.
