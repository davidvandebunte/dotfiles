---
jupytext:
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.6
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Set up [Ubuntu Server](https://ubuntu.com/download/server)

+++

## Select hardware

+++

What's the use case? We can't really say we have just one. See Jupyter as an IDE or text editor; it should come along with every use case. Notebooks should be independent, i.e. capable of being updated separately.

+++

This is analogous to being able to split up a large application into many smaller easier-to-manage applications (applications being analogous to notebooks). Both terraform and docker support quick start-up to make this practical.

+++

### Remote machines

+++

- [Compute – Amazon EC2 Instance Types – AWS](https://aws.amazon.com/ec2/instance-types/)
- [Vantage: Amazon EC2 Instance Comparison](https://instances.vantage.sh/)

+++

For example, see [c5a.2xlarge](https://instances.vantage.sh/aws/ec2/c5a.2xlarge) and [c5.2xlarge](https://instances.vantage.sh/aws/ec2/c5.2xlarge). The AMD option is cheaper but doesn't seem to get as good performance per CPU: [AMD EPYC 7R32 vs Intel Xeon Platinum 8124M @ 3.00GHz](https://www.cpubenchmark.net/compare/3894vs3352/AMD-EPYC-7R32-vs-Intel-Xeon-Platinum-8124M).

```{code-cell} ipython3
reserved_per_hour = 0.2140  # c5.2xlarge
daily = reserved_per_hour * 24
(daily, daily * 30)
```

Per [Supported CPU options for Amazon EC2 instance types - Amazon Elastic Compute Cloud](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/cpu-options-supported-instances-values.html), 8 vPCUs means only 4 cores.

+++

### Local machines

+++

Home machines are cheaper at the cost of additional personal electricity and cooling costs, and a fast network. They're also only cheaper if you have a decent amount of expertise in maintaining machines, so that the investment in hardware you're making will pay off over a long enough period where you make repairs. Everyone needs to maintain home machines to even have a computer with monitors (a thin client), and so home machines that are only a bit more powerful than this minimum are common (amortizing this overhead over a larger resource). This includes not only the fixed costs of the machine, but the ongoing electricity and cooling costs.

+++

For example, it can help to have a multi-GPU setup locally for development. Testing the multi-GPU training (even if you don't let it run) is something you will often want to do locally before starting to take time/money in AWS. Otherwise you'd have expensive GPUs allocated on a machine and barely be using them; even the two smallest GPUs in the cloud can be expensive. It costs more in terms of fixed costs and electricity/cooling to always have a GPU locally, but you're betting that you'll need them once in a while. It'd be time-expensive to constantly switch to new local machines to always minimize dollar costs based on present needs.

+++

There's value in having up to two machines that are nearly the same, such as two deep learning boxes that you can switch between rather than reboot. With this approach, you avoid losing configuration on any particular machine i.e. you properly document everything about a machine before you leave it. If you have this kind of setup, then you really aren't losing much in terms of reproducibility relative to the cloud (it's just much easier to get two equivalent machines in the cloud). We can treat two only-somewhat equal machines as equal to effectively give up performance (sometimes using a slower workstation) in exchange for ensuring this reproducibility.

+++

## EFI

+++

To understand `/boot/efi`, see [EFI system partition](https://en.wikipedia.org/wiki/EFI_system_partition). You'll see `.efi` files in this partition as described in [UEFI booting](https://en.wikipedia.org/wiki/UEFI#UEFIBOOT). The same files will be present on an Ubuntu Live USB. These `.efi` files are essentially executables; see [this Unix & Linux Stack Exchange question](https://unix.stackexchange.com/questions/565615/efi-boot-bootx64-efi-vs-efi-ubuntu-grubx64-efi-vs-boot-grub-x86-64-efi-gru). When you hit `F2` as your system boots to select what disk/usb to boot from, you're not only choosing a device but an EFI path within the device to run. See `efibootmgr -v` for a list of the EFI files that are associated with all the entries you see when you are selecting an option with `F2`. Per [this answer](https://superuser.com/a/1666351/293032), those UUID are PARTUUID.

The output of `df` (e.g. `df -hT`) can be confusing. On your current system, how is `/dev/sda2` mounted to both `/home` and `/`? Wouldn't you think that you would then have the same files in both places? The issue is likely that you're using the `btrfs` filesystem, which can have its own partitions; see [Partitioning - ArchWiki / Btrfs partitioning](https://wiki.archlinux.org/title/Partitioning#Btrfs_partitioning). See also `sudo btrfs subvolume list /`. You'll see the same btrfs partitioning information in `/etc/fstab` when mounting `/` and `/home`.

To understand `grub-update` and `grub-install` see [GNU GRUB Manual 2.06](https://www.gnu.org/software/grub/manual/grub/grub.html).

+++

## Machine-specific notes

+++

See `administrate-hardware.md`. Take notes on hardware-specific problems in e.g. `admin-m2.md`. See also optional instructions in `procure-machine-learning-hardware.md`:
- Disable package updates.
- Get all the GPU memory you can back.
- Save more syslog history

+++

## Server installer

+++

From [20.04 - How do I ssh into the server installer - Ask Ubuntu](https://askubuntu.com/questions/1361827/how-do-i-ssh-into-the-server-installer):

![x](https://lh3.googleusercontent.com/pw/AP1GczOLCbAtSM-NN3gmIlwZMHvPZyvpdY-ECdg3i8qfHOIvqjDUF9HJlDkv6-iFbQSpH1iD3tAeR-VtwZk6_sdd247IHtXPSKSKklj89YxYuk5VXAjeu4lv_89fTqJzD5pjUO7qRSlFwJ1cN5knNk9FIbkh1Q=w885-h615-s-no?authuser=0)

+++

See also "Continue installation remotely using SSH and continue" in [Installation/NetworkConsole - Community Help Wiki](https://help.ubuntu.com/community/Installation/NetworkConsole). The search ["ubuntu 22.04 server installer over ssh"](https://duckduckgo.com/?q=ubuntu+22.04+server+installer+over+ssh&t=newext&atb=v310-1&ia=web) produces [system installation - SSH into Ubuntu 22.04 server installer - Ask Ubuntu](https://askubuntu.com/questions/1439821/ssh-into-ubuntu-22-04-server-installer).

See [Installing Ubuntu 20.04 over SSH - Zameer Manji](https://zameermanji.com/blog/2021/9/9/installing-ubuntu-20-04-over-ssh/). Prefer `fallocate` to `truncate` as described in [command line - How to create a file with a given size in Linux? - Stack Overflow](https://stackoverflow.com/questions/139261/how-to-create-a-file-with-a-given-size-in-linux) when following the instructions in [Automated Server install quickstart | Ubuntu](https://ubuntu.com/server/docs/install/autoinstall-quickstart). Still, these instructions were leading to crashes.

See also [How do I set a custom password with Cloud-init on Ubuntu 20.04? - Stack Overflow](https://stackoverflow.com/questions/61591885/how-do-i-set-a-custom-password-with-cloud-init-on-ubuntu-20-04) for the headaches associated with even encrypting a password. This is also discussed in [Create an autoinstall iso for ubuntu 22.04](https://braydenlee.gitee.io/posts/autoinstall-ubuntu-22.04-jammy/). Despite there being multiple sets of instructions unpacking and repacking an ISO, there are no official instructions on the topic and getting into this seems risky.

+++

## Desktop installer

+++

See [Install Ubuntu 24.04 With Proper Btrfs Setup](https://blackstewie.com/posts/install-ubuntu-24.04-with-proper-btrfs-setup/) for instructions to install on top of a BTRFS filesystem for desktop machines. See [these photos](https://photos.app.goo.gl/teSjJudTc9QKfdPVA) for similar choices for [XFS](https://en.wikipedia.org/wiki/XFS). Although [ZFS](https://en.wikipedia.org/wiki/ZFS) is increasingly popular, it doesn't seem to feature copy-on-write (at least by default).

Notice you don't need to mount anything but `/` even for an XFS filesystem. You can mount `/home` onto a separate disk after the machine is up, see below.

+++

## Mount extra disks

+++

The typical NVMe is disk, at risk of running out of disk space after a machine has been used for some time and has collected a large number of docker images. To identify other disks with available disk space to help alleviate some of this pressure:

+++

```bash
lsblk -p --sort=SIZE
```

+++

To repartition a disk, see `fdisk` and the instructions in [Create a Partition in Linux | DigitalOcean](https://www.digitalocean.com/community/tutorials/create-a-partition-in-linux). This interactive tool is easier to use than e.g. `parted`, but for instructions on how to use that tool see [How To Partition and Format Storage Devices in Linux | DigitalOcean](https://www.digitalocean.com/community/tutorials/how-to-partition-and-format-storage-devices-in-linux). See [GUID Partition Table / Partition type GUIDs](https://en.wikipedia.org/wiki/GUID_Partition_Table#Partition_type_GUIDs) lists some interesting GUIDs in Linux. To check for some of these run `sudo lsblk -O` or filter down to only the columns you care about (from [List partition type GUID's for all disks from command line?](https://unix.stackexchange.com/questions/207661/list-partition-type-guids-for-all-disks-from-command-line)):

```bash
sudo lsblk -p -o NAME,PTTYPE,PARTTYPE
```

In fact, the term "partition" on Linux seems to refer almost always to GPT partitions. You can also run `sudo fdisk` (without the `-l`) to format some disk, and use the `l` command to list all the known GPT GUIDs. See [this answer](https://askubuntu.com/a/464039/612216) for more details on how many tools report these GUIDs differently (as e.g. a `boot` flag).

To check your [GUID Partition Table / MBR variant](https://en.wikipedia.org/wiki/GUID_Partition_Table#MBR_variants), run:

```bash
sudo gdisk -l /dev/sda
```

+++

Use the largest disk for `/home`. An example that additionally tries to save `~/.ssh` (run `sudo -i` to run all these commands as root):

+++

```bash
username=vandebun
home_part=sdd1
mkfs.xfs /dev/${home_part}
echo "/dev/${home_part}   /mnt/aux  xfs  defaults,nofail  0  2" | tee -a /etc/fstab
mkdir /mnt/aux
mount -a
cp -r /home/${username}/.ssh /mnt/aux
chown -R ${username}:${username} /mnt/aux

echo "/dev/${home_part}    /home/${username}  xfs  defaults,nofail  0  2" | tee -a /etc/fstab
mount -a
```

+++

If you have another disk, use it for `/tmp`:

+++

```bash
tmp_part=sdb1
mkfs.xfs /dev/${tmp_part}
echo "/dev/${tmp_part}    /tmp          xfs  defaults,nofail  0  2" | tee -a /etc/fstab
mount -a
chmod 777 /tmp
chmod +t /tmp
```

+++

If you have another disk, use it for `/var` in case you ever get out-of-control logs (as described in [The importance of properly partitioning a disk in Linux](https://www.daniloaz.com/en/the-importance-of-properly-partitioning-a-disk-in-linux/)). See also [Filesystem Hierarchy Standard](https://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard).

+++

In the end, `lsblk -f` should document the system you expect.

+++

## Physical → ssh access

+++

You are often migrating from one host to another, on a private network, with both machines physically next to each other. First, give yourself `ssh` access into the machine your are migrating from (your established desktop) by installing `openssh-server`.

+++

Get the new host's IP address with `hostname -I` or better, reserve an address for it in Deco. Add a name for the computer to `/etc/hosts`:

```
# Permanent IP address configured in router
192.168.68.119   vdd vandebun-dell-7920
192.168.68.105   g5 david-G551JW
```

Then connect to the computer from another machine and run `ssh-copy-id` so you don't need to enter your password regularly.

+++

## Verify reflinks

+++

See also [XFS - Data Block Sharing (Reflink)](https://blogs.oracle.com/linux/post/xfs-data-block-sharing-reflink), which mentions that you should see "reflink=1" after you run `mkfs.xfs`. To doubly-confirm you have CoW support, you can optionally run the following in your home directory. Verify you do not get a `Operation not supported` error:
```bash
$ touch ~/temp.txt && cp --sparse=auto --reflink=always ~/temp.txt ~/temp2.txt && rm ~/temp.txt ~/temp2.txt
```

+++

## Security check

+++

Confirm `nmap g5 vdd` only shows ports you expect on a regular basis. Note that zscaler apparently needs to phone home:

```bash
$ sudo fuser -v 9000/tcp 9010/tcp
                     USER        PID ACCESS COMMAND
9000/tcp:            root      2103811 F.... zstunnel
9010/tcp:            root      2103811 F.... zstunnel
```

It also helps to update `/etc/hosts` so that `nmap 192.168.68.0/24` makes more logical sense, when you're trying to understand what is on your network. Add all the address reservations you can. Regularly check your network for the sake of security as well as being able to potentially do headless installs.

+++

## System binaries

+++

Where do you put custom scripts (such as `aws-rotate-keys`) or downloaded scripts/binaries (such as `kubectl`, `argo`, or `docker-credential-secretservice`)? See both of the excellent answers in [Differences between /bin, /sbin, /usr/bin, /usr/sbin, /usr/local/bin, /usr/local/sbin](https://askubuntu.com/questions/308045/differences-between-bin-sbin-usr-bin-usr-sbin-usr-local-bin-usr-local). In particular, use `man hier` to get a quick local summary of the [Filesystem Hierarchy Standard](https://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard).

+++

## Docker image setup

+++

```bash
# Assuming `git` is installed
RUN git clone https://gitlab.com/davidvandebunte/dotfiles.git \
    && ./dotfiles/setup-dotfiles.sh
```

+++

The dotfiles should include any run_args required to get a container up on the
host (check `.bash_aliases`).

+++

## Install software

+++

```bash
sudo apt install git
cd && git clone git@gitlab.com:davidvandebunte/dotfiles.git
./dotfiles/setup-dotfiles.sh
```

+++

Run `system-set-up` once (on non-desktop machines, see those notes). Run `idempotent-set-up` initially and then regularly as you add packages to the system, recording new packages you want on new systems in this script.

+++

## Credentialed setup

+++

```bash
cd && ./export-secrets
dotf submodule update --init --recursive
user_email="davidvandebunte@gmail.com"
dotf submodule foreach --recursive "git config user.email ${user_email} && git config user.name 'David VandeBunte'"
```

+++

## Optional setup

+++

```bash
git clone git@gitlab.com:davidvandebunte/davidvandebunte.gitlab.io.git
pushd davidvandebunte.gitlab.io/ && git submodule update --init --recursive && popd
user_email="davidvandebunte@gmail.com"
git config user.email ${user_email} && git config user.name 'David VandeBunte'
git submodule foreach --recursive "git config user.email ${user_email} && git config user.name 'David VandeBunte'"
git clone git@gitlab.com:davidvandebunte/personal-notes
pushd personal-notes/ && git submodule update --init --recursive && popd
user_email="davidvandebunte@gmail.com"
git config user.email ${user_email} && git config user.name 'David VandeBunte'
git submodule foreach --recursive "git config user.email ${user_email} && git config user.name 'David VandeBunte'"
```
