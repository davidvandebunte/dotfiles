#!/usr/bin/env sh
set -eux

# Hardcoded version of $CI_REGISTRY_IMAGE in gitlab CI/CD (hardcoded to allow
# local builds).
GITLAB_CI_REGISTRY_IMAGE="registry.gitlab.com/davidvandebunte/dotfiles"
image_tag="${1:-latest}"
image_name="${GITLAB_CI_REGISTRY_IMAGE}:${image_tag}"
DOCKER_BUILDKIT=1 docker build \
    --build-arg BUILDKIT_INLINE_CACHE=1 \
    --cache-from "$image_name" \
    -f dockerfiles/"${image_tag}"/Dockerfile \
    --tag "$image_name" .
