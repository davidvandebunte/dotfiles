#!/usr/bin/env bash
set -euxo pipefail

# Hardcoded version of $CI_REGISTRY_IMAGE in gitlab CI/CD (hardcoded to allow
# local builds).
GITLAB_CI_REGISTRY_IMAGE="registry.gitlab.com/davidvandebunte/dotfiles"
image_tag="${1:-local}"
image_name="${GITLAB_CI_REGISTRY_IMAGE}:${image_tag}"
DOCKER_BUILDKIT=1 docker build \
    --build-arg UNAME="$USER" \
    --build-arg USER_ID="$(id -u)" \
    --build-arg USER_GID="$(id -g)" \
    -f dockerfiles/latest/Dockerfile \
    --tag "$image_name" .
