#!/usr/bin/env sh
set -eux

image_tag=$1
image_name="${CI_REGISTRY_IMAGE}:${image_tag}"
./build/pull-build.sh $image_tag
docker push "$image_name"
