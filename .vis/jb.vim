let SessionLoad = 1
let s:so_save = &g:so | let s:siso_save = &g:siso | setg so=0 siso=0 | setl so=-1 siso=-1
let v:this_session=expand("<sfile>:p")
silent only
silent tabonly
cd ~/source/jb
if expand('%') == '' && !&modified && line('$') <= 1 && getline(1) == ''
  let s:wipebuf = bufnr('%')
endif
let s:shortmess_save = &shortmess
if &shortmess =~ 'A'
  set shortmess=aoOA
else
  set shortmess=aoO
endif
badd +1 notes/cto-book/_toc.yml
badd +14 terraform/set-up-vs
badd +1 notes/start-cto_jb
badd +1 notes/jb-build
badd +58 notes/docker-run-build
badd +0 .gitlab-ci.yml
argglobal
%argdel
edit .gitlab-ci.yml
let s:save_splitbelow = &splitbelow
let s:save_splitright = &splitright
set splitbelow splitright
wincmd _ | wincmd |
vsplit
wincmd _ | wincmd |
vsplit
wincmd _ | wincmd |
vsplit
wincmd _ | wincmd |
vsplit
wincmd _ | wincmd |
vsplit
5wincmd h
wincmd w
wincmd _ | wincmd |
split
wincmd _ | wincmd |
split
2wincmd k
wincmd w
wincmd w
wincmd w
wincmd w
wincmd w
wincmd w
let &splitbelow = s:save_splitbelow
let &splitright = s:save_splitright
wincmd t
let s:save_winminheight = &winminheight
let s:save_winminwidth = &winminwidth
set winminheight=0
set winheight=1
set winminwidth=0
set winwidth=1
exe 'vert 1resize ' . ((&columns * 120 + 360) / 720)
exe '2resize ' . ((&lines * 100 + 96) / 193)
exe 'vert 2resize ' . ((&columns * 119 + 360) / 720)
exe '3resize ' . ((&lines * 26 + 96) / 193)
exe 'vert 3resize ' . ((&columns * 119 + 360) / 720)
exe '4resize ' . ((&lines * 63 + 96) / 193)
exe 'vert 4resize ' . ((&columns * 119 + 360) / 720)
exe 'vert 5resize ' . ((&columns * 119 + 360) / 720)
exe 'vert 6resize ' . ((&columns * 119 + 360) / 720)
exe 'vert 7resize ' . ((&columns * 119 + 360) / 720)
exe 'vert 8resize ' . ((&columns * 119 + 360) / 720)
argglobal
balt terraform/set-up-vs
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let &fdl = &fdl
let s:l = 34 - ((33 * winheight(0) + 95) / 191)
if s:l < 1 | let s:l = 1 | endif
keepjumps exe s:l
normal! zt
keepjumps 34
normal! 018|
wincmd w
argglobal
if bufexists(fnamemodify("notes/docker-run-build", ":p")) | buffer notes/docker-run-build | else | edit notes/docker-run-build | endif
if &buftype ==# 'terminal'
  silent file notes/docker-run-build
endif
balt notes/jb-build
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let &fdl = &fdl
let s:l = 62 - ((61 * winheight(0) + 50) / 100)
if s:l < 1 | let s:l = 1 | endif
keepjumps exe s:l
normal! zt
keepjumps 62
normal! 028|
wincmd w
argglobal
if bufexists(fnamemodify("notes/jb-build", ":p")) | buffer notes/jb-build | else | edit notes/jb-build | endif
if &buftype ==# 'terminal'
  silent file notes/jb-build
endif
balt notes/start-cto_jb
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let &fdl = &fdl
let s:l = 6 - ((5 * winheight(0) + 13) / 26)
if s:l < 1 | let s:l = 1 | endif
keepjumps exe s:l
normal! zt
keepjumps 6
normal! 028|
wincmd w
argglobal
if bufexists(fnamemodify("notes/start-cto_jb", ":p")) | buffer notes/start-cto_jb | else | edit notes/start-cto_jb | endif
if &buftype ==# 'terminal'
  silent file notes/start-cto_jb
endif
balt terraform/set-up-vs
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let &fdl = &fdl
let s:l = 24 - ((23 * winheight(0) + 31) / 63)
if s:l < 1 | let s:l = 1 | endif
keepjumps exe s:l
normal! zt
keepjumps 24
normal! 021|
wincmd w
argglobal
if bufexists(fnamemodify("terraform/set-up-vs", ":p")) | buffer terraform/set-up-vs | else | edit terraform/set-up-vs | endif
if &buftype ==# 'terminal'
  silent file terraform/set-up-vs
endif
balt notes/cto-book/_toc.yml
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let &fdl = &fdl
let s:l = 1 - ((0 * winheight(0) + 95) / 191)
if s:l < 1 | let s:l = 1 | endif
keepjumps exe s:l
normal! zt
keepjumps 1
normal! 0
wincmd w
argglobal
if bufexists(fnamemodify("terraform/set-up-vs", ":p")) | buffer terraform/set-up-vs | else | edit terraform/set-up-vs | endif
if &buftype ==# 'terminal'
  silent file terraform/set-up-vs
endif
balt notes/cto-book/_toc.yml
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let &fdl = &fdl
let s:l = 1 - ((0 * winheight(0) + 95) / 191)
if s:l < 1 | let s:l = 1 | endif
keepjumps exe s:l
normal! zt
keepjumps 1
normal! 0
wincmd w
argglobal
if bufexists(fnamemodify("terraform/set-up-vs", ":p")) | buffer terraform/set-up-vs | else | edit terraform/set-up-vs | endif
if &buftype ==# 'terminal'
  silent file terraform/set-up-vs
endif
balt notes/cto-book/_toc.yml
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let &fdl = &fdl
let s:l = 29 - ((28 * winheight(0) + 95) / 191)
if s:l < 1 | let s:l = 1 | endif
keepjumps exe s:l
normal! zt
keepjumps 29
normal! 017|
wincmd w
argglobal
if bufexists(fnamemodify("notes/cto-book/_toc.yml", ":p")) | buffer notes/cto-book/_toc.yml | else | edit notes/cto-book/_toc.yml | endif
if &buftype ==# 'terminal'
  silent file notes/cto-book/_toc.yml
endif
balt notes/docker-run-build
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let &fdl = &fdl
let s:l = 129 - ((128 * winheight(0) + 95) / 191)
if s:l < 1 | let s:l = 1 | endif
keepjumps exe s:l
normal! zt
keepjumps 129
normal! 020|
wincmd w
3wincmd w
exe 'vert 1resize ' . ((&columns * 120 + 360) / 720)
exe '2resize ' . ((&lines * 100 + 96) / 193)
exe 'vert 2resize ' . ((&columns * 119 + 360) / 720)
exe '3resize ' . ((&lines * 26 + 96) / 193)
exe 'vert 3resize ' . ((&columns * 119 + 360) / 720)
exe '4resize ' . ((&lines * 63 + 96) / 193)
exe 'vert 4resize ' . ((&columns * 119 + 360) / 720)
exe 'vert 5resize ' . ((&columns * 119 + 360) / 720)
exe 'vert 6resize ' . ((&columns * 119 + 360) / 720)
exe 'vert 7resize ' . ((&columns * 119 + 360) / 720)
exe 'vert 8resize ' . ((&columns * 119 + 360) / 720)
tabnext 1
if exists('s:wipebuf') && len(win_findbuf(s:wipebuf)) == 0 && getbufvar(s:wipebuf, '&buftype') isnot# 'terminal'
  silent exe 'bwipe ' . s:wipebuf
endif
unlet! s:wipebuf
set winheight=1 winwidth=20
let &shortmess = s:shortmess_save
let &winminheight = s:save_winminheight
let &winminwidth = s:save_winminwidth
let s:sx = expand("<sfile>:p:r")."x.vim"
if filereadable(s:sx)
  exe "source " . fnameescape(s:sx)
endif
let &g:so = s:so_save | let &g:siso = s:siso_save
set hlsearch
let g:this_session = v:this_session
let g:this_obsession = v:this_session
doautoall SessionLoadPost
unlet SessionLoad
" vim: set ft=vim :
