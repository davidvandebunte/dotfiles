#!/usr/bin/env bash
echo "Sourcing .bash_aliases"

# You found the ninja completion script here:
# https://github.com/ninja-build/ninja/blob/master/misc/bash-completion
#
# You found where it was installed on your local machine with:
# dpkg-query -L ninja-build
NINJA_BASH_COMPLETION="/usr/share/bash-completion/completions/ninja"
if [ -f $NINJA_BASH_COMPLETION ]; then
    source $NINJA_BASH_COMPLETION
    alias nj="nice -n 4 ninja -k100 -l16 -j20"
    alias njd="ionice -c 3 nice -n 4 ninja -k100 -l14 -j20"
    complete -F _ninja_target nj
    complete -F _ninja_target njd
fi

# https://unix.stackexchange.com/questions/177572/how-to-rename-terminal-tab-title-in-gnome-terminal
function set-title() {
  if [[ -z "$ORIG" ]]; then
    ORIG=$PS1
  fi
  TITLE="\[\e]2;$*\a\]"
  PS1=${ORIG}${TITLE}
}
alias stt=set-title

alias vi=nvim

# For kubectl auto completion:
# https://kubernetes.io/docs/reference/kubectl/cheatsheet/
if type kubectl > /dev/null 2>&1; then
    source <(kubectl completion bash)
    alias k=kubectl
    complete -F __start_kubectl k
fi

# Argo autocompletion
if type argo > /dev/null 2>&1; then
    source <(argo completion bash)
fi

alias ipy=ipython

if type tmux > /dev/null 2>&1; then
    source $HOME/completions/bash_completion_tmux.sh
fi

# Turn off ^S and ^Q which you don't feel you need in any contexts:
# https://stackoverflow.com/a/14737844/622049
if [ -t 0 ]; then
    stty sane
    stty stop ''
    stty start ''
    stty werase ''
fi

# Timebox everything you do.
function nt { \
    date
    faketime -f +"$1" date
    # TODO: Print how long you actually worked by catching the Ctrl-C signal (SIGINT?). Also how
    # much time you have left, in case you're just taking a break or you want to see how much time
    # you saved. Perhaps convert this all to Python.
    sleep "$1"
    local WARNING_TEXT
    WARNING_TEXT="$2"
    if [ -z "$WARNING_TEXT" ]; then
        WARNING_TEXT="Out of time!"
    fi
    zenity --warning --title="New timebox!" --text="$WARNING_TEXT" &
    watch paplay /usr/share/sounds/ubuntu/stereo/message.ogg
}

# Similar to the "config" alias suggested in this article:
# https://developer.atlassian.com/blog/2016/02/best-way-to-store-dotfiles-git-bare-repo/
alias dotf='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'

# Quickly switch to the root of the current git repository:
# https://stackoverflow.com/questions/957928/is-there-a-way-to-get-the-git-root-directory-in-one-command/23442470#23442470
#
# You took the name gcd from this comment:
# https://stackoverflow.com/questions/957928/is-there-a-way-to-get-the-git-root-directory-in-one-command/23442470#comment26303681_957928
alias gcd='cd $(git rev-parse --show-toplevel)'

alias pdsql='psql --host=doda.cwa6th4sgj4d.us-east-1.rds.amazonaws.com --port=5432 --username=hereuser'

aws_env() {
    PROFILE="${1:-had-pose-p}"
    echo "setup AWS $PROFILE"
    export AWS_ACCESS_KEY_ID=$(aws configure get aws_access_key_id --profile "$PROFILE");
    export AWS_SECRET_ACCESS_KEY=$(aws configure get aws_secret_access_key --profile "$PROFILE");
    export AWS_DEFAULT_REGION=$(aws configure get region --profile "$PROFILE");
    export AWS_REGION=$(aws configure get region --profile "$PROFILE");
    echo "$PROFILE environment variables exported";
}

da_s3() { WF_ID="$1" ; echo $(echo -n $WF_ID | md5sum | sed "s/\(........\).*/\1/")_${WF_ID} ; }

mr() {
    local cur_br
    cur_br=$(git branch --show-current)
    echo "-o merge_request.create -o merge_request.title=$cur_br"
}

# ACCEPTED SOLUTION
# https://stackoverflow.com/a/34683596/622049
# https://stackoverflow.com/a/40967729/622049
# These (paired) answers requires a manual step every time you need to use ssh,
# etc. The major downside is you may forget to call this before a docker build
# that needs an ssh-agent, and it will break a long way into the build when
# you're no longer looking.
#
# Remember there is no way to update an existing shell's environment without
# a manual step:
# https://stackoverflow.com/questions/8645053/how-do-i-start-tmux-with-my-current-environment#comment20368913_9833996
#
# Although this is manual, the upside is you're forced to think about what ssh
# key you are currently using. That is, you're forced to pull your ssh key
# config rather than assume it is always correct. You will also be more aware
# of when you need an ssh key and when you don't and won't pull it when you
# don't need it. It's less likely some service will use your identity that you
# didn't know uses your identity.
#
# If you have the same problem with any of these other environment variables at
# any point you can take an analogous approach; you can't take an analogous
# approach with the other solution:
# DISPLAY SSH_ASKPASS SSH_AGENT_PID SSH_CONNECTION WINDOWID XAUTHORITY
#
# REJECTED SOLUTIONS
# The primary alternative is described in these articles:
# https://werat.github.io/2017/02/04/tmux-ssh-agent-forwarding.html
# https://stackoverflow.com/a/23187030/622049
#
# In the SO thread, the comments propose the following to get this solution to
# actually work:
# set -g update-environment -r
#
# It doesn't look like -r is a legitimate argument to this function. According
# to the man page, this function takes a list of environment variables:
# https://github.com/tmux/tmux/blob/3.1b/tmux.1#L3491
#
# This article seems to use the function correctly:
# https://blog.mcpolemic.com/2016/10/01/reconciling-tmux-and-ssh-agent-forwarding.html
#
# The default for this setting is provided in this SO answer (with your update
# in comments):
# https://stackoverflow.com/questions/8645053/how-do-i-start-tmux-with-my-current-environment#comment110743122_9833996

# The primary problem with this solution is it makes working on the host more
# difficult, assuming you want to use the same dotfiles everywhere. When you're
# inside of tmux on the host you'll have to pull the correct SSH_AUTH_SOCKET
# somehow or set up a permanent symbolic link from the system SSH_AUTH_SOCKET
# to your other socket in the .ssh directory. Outside tmux would be fine,
# though.
#
# It would also create problems if a host became a remote or vice versa. Your
# work desktop often acts as both; similarly you often need to double ssh from
# one machine and then into another machine.
#
# This solution also creates a single point of failure. It only works as long
# as you only ever need a single key. You often need to use different private
# ssh keys, for example:
# deploy_key hdr.pem jenkins_rsa kor-vandebun.pem mapautomation-p.pem
# That is, see the last paragraph here:
# https://chrisdown.name/2013/08/02/fixing-stale-ssh-sockets-in-tmux.html
#
# It also breaks X11 forwarding if you ever need that; and you maintain another
# config file (.ssh/rc). In general it includes a lot more code than the
# solution you've currently accepted. The accpeted solution seems to be what
# the showenv -s command was designed for.
#
# If you wanted to go back to this solution, add this to your .tmux.conf:
# set -g update-environment "DISPLAY KRB5CCNAME SSH_ASKPASS SSH_AGENT_PID \
#                           SSH_CONNECTION WINDOWID XAUTHORITY"
# setenv -g SSH_AUTH_SOCK ~/.ssh/ssh_auth_sock
#
# https://stackoverflow.com/a/53997724/622049
# This answer also seems reasonable, but you don't want to have your host
# machines have a different dotfile configuration than the machines you ssh
# into.
#
# That is, your host machine (with an SSH key or keys everyone else is using)
# should be able to set up SSH_AUTH_SOCK normally. As pointed out in known
# limitations in this answer, changing the ssh config means you won't be able
# to communicate with the ssh-agent inside of tmux.
alias fixssh='eval $(tmux showenv -s SSH_AUTH_SOCK)'

# Display all "local" branches (not in gitlab but perhaps another remote)
alias display-local-branches='fixssh && git fap && git branch -a | grep -v gitlab'

# Fixssh and display local branches
alias glb='fixssh && display-local-branches'

alias nv='cd /mnt/nas/vandebun'

# https://askubuntu.com/questions/391082/how-to-see-time-stamps-in-bash-history/391087#comment1723728_391087
HISTTIMEFORMAT="%F %T "

if [ -f ~/.bash_personal_aliases ]; then
    source ~/.bash_personal_aliases
fi

alias d=docker

# https://stackoverflow.com/a/71344558/622049
ff() {
    FILES="$1"
    SHA="$2"
    git show "$SHA" -- $FILES | git apply --index -
    git commit -c "$SHA"
}
# Use your mouse to `git show` (redact) and `git cp` commits one at a time.
pff() {
    SHA="$1"
    ff jb/notes "$SHA"
}
sff() {
    SHA="$1"
    ff notes "$SHA"
}

# Short kubernetes aliases
alias k='kubectl'
alias s='kubectl config use-context'

# See comments in learn-tmux.md about stripping whitespace (sw for strip whitespace)
sw() {
    xsel -b | sed 's/  *$//' | xsel -b
}

# Mount several history files as part of docker run --rm -it
#
# Forward the TERM environment variable so you have 256 colors inside of docker
# containers. Whether you start a docker container outside or inside tmux will
# determine what TERM you forward (xterm-256color or screen-256color):
# https://github.com/tmux/tmux/wiki/FAQ
it() {
    echo "--rm -it" \
         "--mount type=bind,source=$HOME/.viminfo,target=$HOME/.viminfo" \
         "--mount type=bind,source=/etc/localtime,target=/etc/localtime,ro" \
         "--mount type=bind,source=$HOME/.bash_history,target=$HOME/.bash_history" \
         "--hostname $(hostname)" \
         "--env TERM"
}

# See an example of this pattern here:
# https://docs.docker.com/engine/reference/commandline/run/#mount-volume--v---read-only
#
# Mount and work in the same directory (defaulting to pwd).
mw() {
    local work_dir
    work_dir=${1:-$(pwd)}
    echo "--mount type=bind,source=${work_dir},target=${work_dir} --workdir ${work_dir}"
}

gmw() {
    local top_level
    top_level=$(git rev-parse --show-toplevel)
    local gitdir=$(git rev-parse --git-common-dir)
    echo "--mount type=bind,source=$(realpath $gitdir),target=$(realpath $gitdir) --mount type=bind,source=${top_level:?null or unset top_level},target=/repo --workdir /repo/$(git rev-parse --show-prefix)"
}

startmin() {
    docker run $(it) $(gmw) registry.gitlab.com/davidvandebunte/minimal-environment:davidvandebunte
}

# The following line is part of tmuxinator setup:
# https://github.com/tmuxinator/tmuxinator
source ~/.tmuxinator.bash

# See:
# https://docs.google.com/document/d/1vQPrNDdwNSWZHHZF7cDg7bwpjtAAzxykyp5-TM6UrSI/edit
alias nmk="nice -n 2 make -k -j20"
complete -W "\`grep -oE '^[a-zA-Z0-9_.-]+:([^=]|$)' Makefile | sed 's/[^a-zA-Z0-9_.-]*$//'\`" nmk
